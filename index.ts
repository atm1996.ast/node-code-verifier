import express, { Express,Request,Response } from "express"; 
import dotenv from "dotenv";

//Configuration
dotenv.config();

// Create Express APP
const app:Express = express();
const port:number | string = process.env.PORT || 8000;

/**
 * 
 */
app.get('/',(req:Request,res:Response) => {
    res.send('APP Express + TS');
})

app.get('/hello',(req:Request,res:Response) => {
    res.send('hello word!');
})

app.listen(port,() => {
    console.log(`Express running: http://localhost:${port}`);
})


